title "YUM Zabbix Repository"

describe yum.repo('zabbix') do
    it { should exist }
    it { should be_enabled }
end

