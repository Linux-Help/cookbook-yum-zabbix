require 'spec_helper'

describe 'yum-zabbix::default' do
    context 'Creates Zabbix YUM repository and enables it.' do
        let(:chef_run) do
            ChefSpec::SoloRunner.new do |node|
                # Create a new environment (you could also use a different :let block or :before block)
                env = Chef::Environment.new
                env.name 'unit_test'

                # Stub the node to return this environment
                allow(node).to receive(:chef_environment).and_return(env.name)

                # Stub any calls to Environment.load to return this environment
                allow(Chef::Environment).to receive(:load).and_return(env)

                # Stubbing out fqdn node attribute
                node.automatic['fqdn'] = 'unit.testing.stub'
            end.converge(described_recipe)
        end

        #before(:each) do
          #stub_command("rpm -qa zabbix-release-2.4-1.el6.noarch ").and_return(false)
          #allow(Chef::EncryptedDataBagItem).to receive(:load).with('odhp_credentials', 'credentials').and_return(
          #{
          #    'zabbix' => {
          #        'users' => {
          #            'unit_test_env' => {
          #                'postgres' => {
          #                    'username' => 'postgres_username',
          #                    'password' => 'postgres_password'
          #                }
          #            }
          #        }
          #    }
          #})
        #end

        it 'installs the zabbix repository' do
          expect(chef_run).to create_yum_repository('zabbix')
        end
	end
end

