default['yum']['zabbix']['repositoryid'] = 'zabbix'
default['yum']['zabbix']['description'] = 'Zabbix Official Repository - $basearch'
default['yum']['zabbix']['release_repo'] = '3.0'
case node['platform']
when 'amazon'
    default['yum']['zabbix']['baseurl'] = "http://repo.zabbix.com/zabbix/#{node['yum']['zabbix']['release_repo']}/rhel/6/$basearch/"
    default['yum']['zabbix']['gpgkey'] = 'http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX'
else
    default['yum']['zabbix']['baseurl'] = "http://repo.zabbix.com/zabbix/#{node['yum']['zabbix']['release_repo']}/rhel/#{node['platform_version'].to_i}/$basearch/"
    if node['yum']['zabbix']['release_repo'].to_f > 3.0
        default['yum']['zabbix']['gpgkey'] = 'http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX-A14FE591'
    else
        default['yum']['zabbix']['gpgkey'] = 'http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX'
    end
end
default['yum']['zabbix']['enabled'] = true
default['yum']['zabbix']['managed'] = true
default['yum']['zabbix']['gpgcheck'] = true
default['yum']['zabbix']['make_cache'] = true
