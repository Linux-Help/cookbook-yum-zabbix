name             'yum-zabbix'
maintainer       'Eric Renfro'
maintainer_email 'psi-jack@linux-help.org'
license          'Apache 2.0'
description      'Installs and configures the Zabbix Yum Repository'
long_description ''
version          '1.0.1'
issues_url       'https://git.linux-help.org/Linux-Help/cookbook-yum-zabbix/issues'
source_url       'https://git.linux-help.org/Linux-Help/cookbook-yum-zabbix'

%w{amazon centos redhat oracle scientific}.each do |os|
	supports os, '>= 6.0.0'
end

depends 'compat_resource', '>= 12.16.3'
depends 'yum-epel', '>= 2.1.1'

chef_version '>= 12.1'

